$(function() {
    'use strict';

    const socket = io();

    let nameInput = prompt("Enter a username");
    let emailInput = prompt("Enter your email");
    let newInput = {
        name: nameInput.replace(/<(|\/|[^>\/bi]|\/[^>bi]|[^\/>][^>]+|\/[^>][^>]+)>/g, ''),
        email: emailInput.replace(/<(|\/|[^>\/bi]|\/[^>bi]|[^\/>][^>]+|\/[^>][^>]+)>/g, '')
    };

    function getTime() {
        let date = new Date();
        let hours = date.getHours();
        let minutes = ((date.getMinutes() < 10) ? '0' : '') + date.getMinutes();
        return `${hours}:${minutes}`;
    }

    function setUser() {
        socket.emit('setUser', {
            name: newInput.name,
            email: newInput.email
        });
    }
    setUser();
    // Get's message and emits it to the server in the form of an object literal with the username
    $('#message-input').keyup(function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            sendMessage();
            $('#message-input').val('');
        }
    });
    let chatItemTemplate = function(data) {
        return (`
<div class="chat-item">
  <div class="message-avatar">
    <img src="${data.user.gravatar}" alt="" />
  </div>
  <div class="message-details">
    <div class="message-header">
      <div class="message-username">${data.user.username}</div>
      <div class="message-date">${getTime()}</div>
    </div>
    <div class="message-text">${data.message}</div>
  </div>
</div>`)};

    let joinMessageTemplate = function(data) {
        return (`<div class="chat-item">
  <div class="message-avatar">
    <img src="${data.user.gravatar}" alt="" />
  </div>
  <div class="message-details">
    <div class="message-header">
      <div class="message-username">${data.user.username}</div>
      <div class="message-date">${getTime()}</div>
    </div>
    <div class="message-text">${data.message}</div>
  </div>
</div>`);
    };

    function sendMessage() {
        let msg = $('#message-input').val();
        if (msg) {
            socket.emit('message', {
                message: msg,
                user: user
            });
        }
    }

    let user = null;
    socket.on('userExists', (data) => {
        alert(data);
    });

    socket.on('userSet', (data) => {
        user = data.user;
        $("#room").css("display", "inline");
        $(".header-avatar").append(`<img src="${data.user.gravatar}"/>`)

        socket.on('broadcast', (data) => {
            $('#message-feed').append(joinMessageTemplate(data));
        });

        // Handles 'new message' event
        socket.on('new message', (data) => {
            // Verifies user's existence
            if (user) {
                // Adds user's message to feed
                $('#message-feed').append(chatItemTemplate(data));
            }
        });
    });
});