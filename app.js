"use strict";

const express = require('express');
const ejs = require('ejs');
const http = require('http');
const gravatar = require('gravatar');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

const port = 8080;
const host = '0.0.0.0';

app.use(express.static('public'));

app.set('view engine', 'ejs');
app.set('views', './views');

/**************************
 * Socket.IO Configuration *
 **************************/

let db = [{
    username: 'test',
    gravatar: '//www.gravatar.com/avatar/61024896f291303615bcd4f7a0dcfb74'
}];
let newUser = {};
// Handles connections to default namespace: '/'
io.on('connection', (socket) => {
    function usernameInDB(value, array) {
        for (var i = 0; i < db.length; i++) {
            if (db[i].username === value) {
                return true;
            }
        }
    }
    // Handles setUser event
    socket.on('setUser', (data) => {
        let form = data;
        newUser = {
            username: form.name,
            gravatar: gravatar.url(form.email)
        };
        // Checks if username already exists in 'db'
        if (!usernameInDB(form.name, db)) {
            db.push(newUser);
            console.log(`${newUser.username} has joined.`);
            io.sockets.emit('broadcast', {
                user: newUser,
                message: `${newUser.username} has joined.`
            });
            socket.emit('userSet', {
                user: newUser
            });
        }
        else {
            socket.emit('userExists', `The username ${newUser.username} is taken.`);
        }
    });

    // Handles message event
    socket.on('message', (data) => {
        io.sockets.emit('new message', data);
    });

    // Handles disconnections
    socket.on('disconnect', (socket) => {
        for (let i = 0; i < db.length; i++) {
            if (db[i].username === newUser.username) {
                db.splice(i, 1);
            }
        }
        console.log(`${newUser.username} has disconnected.`);
    });
});

app.get('/', (req, res) => res.render('index'));

server.listen(port, host, () => console.log(`Listening on port ${port}`));
