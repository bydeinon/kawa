# Kawa | 川

Socket.IO test chat application using Express on the server-side and JQuery on the client-side.

![preview](kawa-snapshot.PNG "Kawa")